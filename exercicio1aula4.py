# encoding: utf-8

import csv
from base import Matriz

Novo = open('listaAdj.csv','w')
class Aresta:
    def inserirAresta(self, matriz,x,y):
        matriz[x][y] = 1
        matriz[y][x] = 1

        return matriz


class Adjacencia:
    def vertAdj(self,x):
        adj = []
        for i in range(len(matriz[x])):
            if matriz[x][i]==1:
                adj.append(i)
        return adj

    def listaAdj(self):
        lista = []
        for i in range(len(matriz)):
            concatena = []
            concatena.append(i)
            concatena.append(self.vertAdj(i))
            lista.append(concatena)
        return lista


valor = raw_input('Olá! Digite a ordem da matriz: ')
print ('-------------------------------------------------------------')
valor = int(valor)

matrizClass = Matriz(valor)
arestaClass = Aresta()
 
matriz = matrizClass.imprimeMatriz()

sairWhile = ''

while sairWhile != 's':
    print ('-------------------------------------------------------------')
    x, y = input('Aresta (x,y): ')
    print ('-------------------------------------------------------------')
    x = int(x)
    y = int(y)

    matriz = arestaClass.inserirAresta(matriz,x,y)
    matrizClass.imprimeMatriz()

    print ('-------------------------------------------------------------')
    sairWhile = raw_input('Digite "s" para parar de adicionar arestas: ')

print ('*************************************************************')
print ('*************************************************************')
print ('')

adjClass = Adjacencia()

sairWhile = ''

while sairWhile != 's':
    valor = raw_input('Digite o vértice a ser analisada a existência de adjacência: ')
    print ('-------------------------------------------------------------')
    valor = int(valor)
    vert = adjClass.vertAdj(valor)

    vertLimpa = str(vert)
    vertLimpa = vertLimpa.replace('[', '')
    vertLimpa = vertLimpa.replace(']', '')
    print ('Vértices adjacentes à ' + str(valor) + ': ' + vertLimpa)
    del vertLimpa
    
    print ('Grau do vértice: ' + str(len(vert)))
    print ('-------------------------------------------------------------')

    v1, v2 = input('Vértices (v1,v2): ')
    print ('-------------------------------------------------------------')

    verify = False
    for i in adjClass.vertAdj(v1):
        if i == v2:
            verify = True

    print ('-------------------------------------------------------------')
    print (str(v1) + ' é adjacente à ' + str(v2) + '?' + str(verify))

    print ('-------------------------------------------------------------')
    sairWhile = raw_input('Digite "s" para parar de verificar as adjacências: ')
    if sairWhile != 's':
        print ('-------------------------------------------------------------')

print ('Lista de Adjacências: ')
print (adjClass.listaAdj())
print ('-------------------------------------------------------------')

Novo.write(str(adjClass.listaAdj()))
Novo.close()

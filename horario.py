semana = [["segunda"],["terca"],["quarta"],["quinta"],["sexta"]]

professores = [("elo","segunda","terca"),("faulindo","segunda","terca"),("veio_do_up","quarta","quinta"),("borbao","quarta","quinta"),("gelado","segunda","sexta"),("tinhoso","terca","quarta","sexta")]

quarto = [["ingles",1,"elo"],["manejos",1,"faulindo"],["redes",2,"tinhoso"],["empreendedorismo",1,"veio_do_up"],["algoritmos",3,"borbao"],["web semantica",2,"gelado"]]

terceiro = []

segundo = []

primeiro = []

def horario(semana,mat,x=0):
    if len(semana) > x:
        insMateria(semana[x],mat)
        return horario(semana,mat,x+1)
    else:
        return semana

def verificaProfessor(dia,professor):
    if dia in professor:
        return True
    else:
        return False

def insMateria(dia,materias,x=0):
    if len(dia) < 3:
        if materias[x][1] == 0:
            return insMateria(dia,materias,x+1)
        else:
            if verificaProfessor(dia[0],varreProf(materias[x][2],professores)):
                dia.append(materias[x][0])
                materias[x][1] -= 1
                return insMateria(dia,materias,x)
            else: 
                return insMateria(dia,materias,x+1)
    else:
        return dia

def varreProf(nome,lista,x=0):
    if nome == lista[x][0]:
        return lista[x]
    else:
        return varreProf(nome,lista,x+1)

horarioQuarto = horario(semana,quarto)

print(horarioQuarto)
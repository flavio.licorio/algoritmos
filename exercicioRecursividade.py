

def mult(x,y,z=0):
    if y == 1: return x
        
    else: return x + mult(x,y-1)

def fat(x,y):
    if y == 1: return x
    
    else: return x * fat(x,y-1)

def soma(x):
    if x == 0:
        return x
    else:
        return x + soma(x-1)

def primeiro(x):
    if x<10: return x
    else: return int(primeiro(x/10))

def lista(x,y):
    if y not in x: return x
    else: 
        x.pop(x.index(y))
        return lista(x,y)

def perfeito(x,y=1,soma=0):
    if x > y:
        if x%y == 0:
            soma += y
        return perfeito(x,y+1,soma)
    elif x == soma: return True
    else: return False


# print(mult(10,5))
# print(fat(2,3))
# print(soma(3))
# print(primeiro(2500))
# print(lista([1,2,3,5,5,6,4],3))
print(perfeito(6))
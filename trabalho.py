def verticesAdj(grafo, vertice):
	adj = []
	for i in range(1, len(grafo[vertice])):
		adj.append(grafo[vertice][i])
	return adj

def agendarSemana(grafo,semana,fila,visitados,professores_materias, vertice):
    if vertice < 10 and vertice not in visitados:
        fila.append(grafo[vertice])
        while len(fila) >0:
            v = fila.pop(0)
            adj = verticesAdj(grafo,v[0])
            for vizinho in range(len(adj)):
                if v[0] not in visitados:
                    aula = verificaAula(encontraValor(quartoTermo,adj[vizinho]),professores_materias)
                    if aula != None:    
                        visitados.append(grafo[vertice][0])
                        dia = []
                        dia.append(v[0])
                        dia.append(aula)
                        semana.append(dia)
                    elif vizinho+1 == len(adj):
                        visitados.append(grafo[vertice][0])

        return agendarSemana(grafo,semana,fila,visitados,professores_materias,vertice+1)
    elif len(visitados) < 10:
        return agendarSemana(grafo,semana,fila,visitados,professores_materias, 0)
    else:
        return semana
def encontraValor(dicionario,valor):
    return dicionario.get(valor)

def verificaAula(professor,lista):
    for obj in lista:
        if professor in obj and obj[1][1] != 0:
            obj[1][1] -= 2
            return obj[1][0]

quartoTermo = {
    0 : 'segundaA',
    1 : 'SegundaD',
    2 : 'tercaA', 
    3 : 'tercaD',
    4 : 'quartaA',
    5 : 'quartaD',
    6 : 'quintaA',
    7 : 'quintaD',
    8 : 'sextaA',
    9 : 'sextaD',
    10 : 'faulin',
    11 : 'ettore',
    12 : 'vitor',
    13 : 'eloiza',
    14 : 'cesar',
    15 : 'kleber'
}

semana_professores = [
    [0,10,11,12],
    [1,10,11],
    [2,11,13,14],
    [3,13,14],
    [4,11,10,12],
    [5,11,15,12],
    [6,10],
    [7,12,14],
    [8,15,10],
    [9,15],
    [10,0,1,4,8,6],
    [11,0,1,2,4,5],
    [12,0,4,5,7],
    [13,2,3],
    [14,2,3,7],
    [15,5,8,9]
]


# print(encontraValor(quartoTermo,0))
# print(verificaAula('faulin',professores_materias))
def tentar(x):
    semana = []
    fila = []
    visitados= []
    professores_materias= [
        ['faulin',['solos',2]],
        ['ettore',['redes',4]],
        ['vitor',['algoritmos',6]],
        ['eloiza',['ingles',2]],
        ['cesar',['empreendedorismo',2]],
        ['kleber',['semantica',4]]
    ]
    agenda = agendarSemana(semana_professores,semana,fila,visitados,professores_materias,x)
    if len(agenda) == 10:
        return agenda
    else: 
        faltantes = []
        for professor in professores_materias:
            if professor[1][1] != 0:
                faltantes.append(professor)
        return "nao conseguiu agendar, faltou ",faltantes


for y in range(10):
    print(tentar(y))
